<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function comfort_scripts()
{
	// CSS
	wp_enqueue_style('comfort-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('comfort-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('comfort-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//code.jquery.com/jquery-3.6.0.min.js', array(), '3.6.0', true);
	wp_enqueue_script('jquery');
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('comfort-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'comfort_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function comfort_gutenberg_scripts()
{
	// Web Fonts
	wp_enqueue_style('comfort-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'comfort_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function comfort_setup()
{
	// Enabling translation support
	$textdomain = 'comfort';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 46,
		'width'       => 46,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'comfort'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Add Custom Header Support
	add_theme_support('custom-header', array(
		'default-image'      => 'https://via.placeholder.com/2560x400/000000/ffffff',
		'default-text-color' => 'fff',
		'width'              => 2560,
		'height'             => 400,
		'flex-width'         => true,
		'flex-height'        => true,
	));

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'comfort'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Cultured', 'comfort'),
			'slug'  => 'cultured',
			'color' => '#f2f2f2ff',
		),
		array(
			'name'  => __('Silver Sand', 'comfort'),
			'slug'  => 'silver-sand',
			'color' => '#bcc5cfff',
		),
		array(
			'name'  => __('Cadet Grey', 'comfort'),
			'slug'  => 'cadet-grey',
			'color' => '#8d9aa8ff',
		),
		array(
			'name'  => __('Prussian Blue', 'comfort'),
			'slug'  => 'prussian-blue',
			'color' => '#263a4fff',
		),
		array(
			'name'  => __('French Blue', 'comfort'),
			'slug'  => 'french-blue',
			'color' => '#0f76bdff',
		),
		array(
			'name'  => __('Orange Crayola', 'comfort'),
			'slug'  => 'orange-crayola',
			'color' => '#f3783aff',
		),
		array(
			'name'  => __('Orange Red', 'comfort'),
			'slug'  => 'orange-red',
			'color' => '#f3722cff',
		),
		array(
			'name'  => __('Fire Engine Red', 'comfort'),
			'slug'  => 'fire-engine-red',
			'color' => '#c12026ff',
		),
		array(
			'name'  => __('Black', 'comfort'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'comfort'),
			'size' => 16,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'comfort'),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'comfort'),
			'size' => 24,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'comfort'),
			'size' => 40,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'comfort'),
			'size' => 50,
			'slug' => 'huge',
		),
	));

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/media-text', array(
		'name' => 'content-on-box__blue',
		'label' => __('Content on Box Blue', 'comfort'),
	));

	register_block_style('core/media-text', array(
		'name' => 'content-on-box__red',
		'label' => __('Content on Box Red', 'comfort'),
	));

	register_block_style('core/media-text', array(
		'name' => 'content-overlap__blue',
		'label' => __('Content Overlap Blue', 'comfort'),
	));

	register_block_style('core/media-text', array(
		'name' => 'content-overlap__red',
		'label' => __('Content Overlap Red', 'comfort'),
	));
}
add_action('after_setup_theme', 'comfort_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function comfort_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'comfort'),
		'id' => 'comfort-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'comfort'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'comfort'),
		'id' => 'comfort-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'comfort'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'comfort'),
		'id' => 'comfort-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'comfort'),
	)));

	// Footer #4
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #4', 'comfort'),
		'id' => 'comfort-sidebar-footer-4',
		'description' => __('The widgets in this area will be displayed in the fourth column in Footer.', 'comfort'),
	)));

	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'comfort'),
		'id' => 'comfort-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'comfort'),
	)));
}

add_action('widgets_init', 'comfort_sidebars');


/**
 * Remove website field from comment form
 *
 */
function comfort_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'comfort_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
