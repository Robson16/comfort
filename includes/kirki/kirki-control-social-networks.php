<?php

new \Kirki\Section(
	'comfort_section_social_networks',
	array(
		'title'       => esc_html__('Social Networks', 'comfort'),
		'description' => esc_html__('Social networks pages.', 'comfort'),
		'priority'    => 160,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'comfort_setting_facebook',
		'label'    => esc_html__('Facebook Page URL', 'comfort'),
		'section'  => 'comfort_section_social_networks',
		'default'  => 'https://www.facebook.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'comfort_setting_instagram',
		'label'    => esc_html__('Instagram Page URL', 'comfort'),
		'section'  => 'comfort_section_social_networks',
		'default'  => 'https://www.instagram.com/',
		'priority' => 10,
	)
);

new \Kirki\Field\URL(
	array(
		'settings' => 'comfort_setting_twitter',
		'label'    => esc_html__('Twitter Page URL', 'comfort'),
		'section'  => 'comfort_section_social_networks',
		'default'  => 'https://twitter.com/',
		'priority' => 10,
	)
);
